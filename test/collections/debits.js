const debits = {
  success: [{
      cardId: '123456',
      code: '123456',
      value: 123
    },
    {
      cardId: '123456',
      code: '456789',
      value: 12
    },
    {
      cardId: '123456',
      code: '456789',
      value: 16
    }
  ],
  error: []
};

module.exports = debits;
