const transportLines = {
  success: [{
      code: '123456',
      origin: 'Savassi',
      destination: 'Centro'
    },
    {
      code: '456789',
      origin: 'Centro',
      destination: 'Pampulha'
    },
    {
      code: '1599357',
      origin: 'Pampulha',
      destination: 'Buritis'
    }
  ],
  error: [{
    origin: 'SP',
    destination: 'RJ'
  }]
};

module.exports = transportLines;
