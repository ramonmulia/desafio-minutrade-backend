const chai = require('chai');
const request = require('supertest-as-promised');
const app = require('../../../src/server');
const debitsCollection = require('../../collections/debits');
const transportLineCollections = require('../../collections/transportLines');
const passengersCollections = require('../../collections/passengers');
const TrasnportLineService = require('../../../src/transportLines/transportLines.service');
const PassengersService = require('../../../src/passengers/passengers.service');
const MachinesService = require('../../../src/machines/machines.service');
const machineCollections = require('../../collections/machines');
const Cryptography = require('../../../src/cryptography/cryptography.service');

const transportLineService = TrasnportLineService();
const passengersService = PassengersService();
const machinesService = MachinesService();
const cryptographyService = Cryptography();

const expect = chai.expect;
const DEBITS_ENDPOINT = '/api/debits';

describe('Debits - Integration Tests - Sucess Cases', () => {
  it('Should create debit correctly', (done) => {
    const debit = debitsCollection.success[0];
    const machine = machineCollections.success[0];

    debit.cardId = 'XnjsoP12';
    debit.code = '123456';

    const token = cryptographyService.encodeToken({ name: machine.name, code: machine.code });

    createDefaultEnvironment()
      .then(() => {
        request(app)
          .post(`${DEBITS_ENDPOINT}`)
          .set('Authorization', `Bearer ${token}`)
          .send(debit)
          .then((res) => {
            expect(res.statusCode).to.equal(201);
            expect(res.body.id).to.not.be.undefined;
            done();
          })
          .catch((err) => {
            done(err);
          });
      })
      .catch((err) => {
        console.log('Error', err);
      });
  });

  function createDefaultEnvironment() {
    return new Promise((resolve) => {
      const tl = transportLineCollections.success[0];
      const psg = passengersCollections.success[0];
      const machine = machineCollections.success[0];

      psg.cardId = 'XnjsoP12';
      tl.code = '123456';

      machinesService.create(machine)
        .then(() => passengersService.create(psg))
        .then(() => transportLineService.create(tl))
        .then(resolve);
    });
  }
});
