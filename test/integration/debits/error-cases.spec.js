const app = require('../../../src/server');
const chai = require('chai');
const request = require('supertest-as-promised');
const debitCollections = require('../../collections/debits');
const passengersCollections = require('../../collections/passengers');
const transportLineCollections = require('../../collections/transportLines');
const DebitService = require('../../../src/debits/debits.service');
const PassengersService = require('../../../src/passengers/passengers.service');
const TrasnportLineService = require('../../../src/transportLines/transportLines.service');

const expect = chai.expect;
const DEBITS_ENDPOINT = '/api/debits';

const transportLineService = TrasnportLineService();
const passengersService = PassengersService();
const debitService = DebitService();

describe('Debits - Integration Tests - Error Cases', () => {
  it('Should not return debit when passenger is not authenticated', (done) => {
    const debit = debitCollections.success[0];

    debit.cardId = 'XnjsoP12';
    debit.code = '123456';

    createDefaultEnvironment()
      .then(() => {
        debitService.create(debit)
          .then(() => {
            request(app)
              .get(DEBITS_ENDPOINT)
              .set('Authorization', 'Bearer vs1d23s1d32s1vs32dv1sd32v1')
              .expect(401)
              .then((res) => {
                expect(res.statusCode).to.equal(401);
                done();
              })
              .catch((err) => {
                done(err);
              });
          });
      });
  });
});

function createDefaultEnvironment() {
  return new Promise((resolve) => {
    const tl = transportLineCollections.success[0];
    const psg = passengersCollections.success[0];

    psg.cardId = 'XnjsoP12';
    tl.code = '123456';

    passengersService.create(psg)
      .then(() => transportLineService.create(tl))
      .then(resolve);
  });
}
