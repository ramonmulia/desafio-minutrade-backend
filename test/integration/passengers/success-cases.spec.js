const chai = require('chai');
const request = require('supertest-as-promised');
const app = require('../../../src/server');
const passengersCollections = require('../../collections/passengers');
const usersCollections = require('../../collections/users');
const UsersService = require('../../../src/users/users.service');

const usersService = UsersService();
const expect = chai.expect;
const PASSENGERS_ENDPOINT = '/api/passengers';

describe('Passengers - Unit Tests - Sucess Cases', () => {
  it('Should return Passenger correctly', (done) => {
    const passenger = passengersCollections.success[0];
    const user = usersCollections.success[0];

    user.password = '123456';

    usersService.create(user)
      .then(() => {
        request(app)
          .post(`${PASSENGERS_ENDPOINT}`)
          .set('Authorization', `Basic ${Buffer.from(`${user.code}:${user.password}`).toString('base64')}`)
          .send(passenger)
          .then((res) => {
            expect(res.statusCode).to.equal(201);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
  });

  it('Should Passenger has token', (done) => {
    const passenger = passengersCollections.success[0];
    const user = usersCollections.success[0];

    user.password = '123456';

    usersService.create(user)
      .then(() => {
        request(app)
          .post(`${PASSENGERS_ENDPOINT}`)
          .send(passenger)
          .set('Authorization', `Basic ${Buffer.from(`${user.code}:${user.password}`).toString('base64')}`)
          .then((res) => {
            expect(res.body.token).to.not.be.undefined;
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
  });
});
