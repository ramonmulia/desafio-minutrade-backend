const chai = require('chai');
const transportLineCollections = require('../../collections/transportLines');
const usersCollections = require('../../collections/transportLines');
const request = require('supertest-as-promised');
const app = require('../../../src/server');
const UsersService = require('../../../src/users/users.service');

const usersService = UsersService();
const TRANSPORT_LINE_ENDPOINT = '/api/transportlines';
const expect = chai.expect;

describe('Transport Line - Integration Tests - Sucess Cases', () => {
  it('Should return Transport Line correctly', (done) => {
    const tl = transportLineCollections.success[0];
    const user = usersCollections.success[0];

    user.password = '123456';

    usersService.create(user)
      .then(() => {
        request(app)
          .post(`${TRANSPORT_LINE_ENDPOINT}`)
          .set('Authorization', `Basic ${Buffer.from(`${user.code}:${user.password}`).toString('base64')}`)
          .send(tl)
          .then((res) => {
            expect(res.statusCode).to.equal(201);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
  });
});
