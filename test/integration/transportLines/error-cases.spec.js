const chai = require('chai');
const transportLineCollections = require('../../collections/transportLines');
const request = require('supertest-as-promised');
const app = require('../../../src/server');
const UsersService = require('../../../src/users/users.service');
const usersCollections = require('../../collections/transportLines');

const usersService = UsersService();
const TRANSPORT_LINE_ENDPOINT = '/api/transportlines';
const expect = chai.expect;

describe('Transport Line - Integration Tests - Error Cases', () => {
  it('Should return error when required fields not informed', (done) => {
    const tl = transportLineCollections.error[0];
    const user = usersCollections.success[0];

    user.password = '123456';

    usersService.create(user)
      .then(() => {
        request(app)
          .post(`${TRANSPORT_LINE_ENDPOINT}`)
          .set('Authorization', `Basic ${Buffer.from(`${user.code}:${user.password}`).toString('base64')}`)
          .send(tl)
          .then((res) => {
            expect(res.statusCode).to.equal(400);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
  });
});
