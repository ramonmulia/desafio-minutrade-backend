const chai = require('chai');
const PassengersService = require('../../../src/passengers/passengers.service');
const passengersCollections = require('../../collections/passengers');

const expect = chai.expect;
const passengersService = PassengersService();

describe('Passengers - Unit Tests - Error Cases', () => {
  it('Should return email error message', (done) => {
    const passenger = passengersCollections.success[0];

    passengersService.create(passenger)
      .then(() => passengersService.create(passenger))
      .catch((error) => {
        expect(error.status).to.equal(400);
        expect(error.error).to.equal('E-mail já existente');
        done();
      });
  });

  it('Should return cardId error message', (done) => {
    const passenger1 = passengersCollections.success[0];
    const passenger2 = Object.assign({}, passenger1);

    passenger2.email = 'teste@123.com';
    passengersService.create(passenger1)
      .then(() => passengersService.create(passenger2))
      .catch((error) => {
        expect(error.status).to.equal(400);
        expect(error.error).to.equal('CardId já existente');
        done();
      });
  });
});
