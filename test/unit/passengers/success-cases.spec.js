const chai = require('chai');
const PassengersService = require('../../../src/passengers/passengers.service');
const passengersCollections = require('../../collections/passengers');

const expect = chai.expect;
const passengersService = PassengersService();

describe('Passengers - Unit Tests - Sucess Cases', () => {
  it('Should return Passenger correctly', (done) => {
    const passenger = passengersCollections.success[0];
    passengersService.create(passenger)
      .then((result) => {
        expect(result.name).to.equal('Teste');
        done();
      })
      .catch(console.log)
  });
});
