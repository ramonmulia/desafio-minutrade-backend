const chai = require('chai');
const DebitsService = require('../../../src/debits/debits.service');
const TransportLineService = require('../../../src/transportLines/transportLines.service');
const debitsCollections = require('../../collections/debits');
const PassengersService = require('../../../src/passengers/passengers.service');
const passengerCollections = require('../../collections/passengers');
const transportLineCollections = require('../../collections/transportLines');

const expect = chai.expect;
const debitsService = DebitsService();
const transportLineService = TransportLineService();
const passengersService = PassengersService();

describe('Debits - Unit Tests - Error Cases', () => {
  it('Should not Debit when greater than 6 per day', (done) => {
    const debit = debitsCollections.success[0];
    const passenger = passengerCollections.success[0];
    const tl1 = transportLineCollections.success[0];

    passengersService.create(passenger)
      .then(() => transportLineService.create(tl1))
      .then(() => debitsService.create(debit))
      .then(() => debitsService.create(debit))
      .then(() => debitsService.create(debit))
      .then(() => debitsService.create(debit))
      .then(() => debitsService.create(debit))
      .then(() => debitsService.create(debit))
      .then(() => debitsService.create(debit))
      .catch((err) => {
        expect(err.status).to.equal(400);
        done();
      });
  });
});
