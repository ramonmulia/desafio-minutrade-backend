const chai = require('chai');
const DebitsService = require('../../../src/debits/debits.service');
const TransportLineService = require('../../../src/transportLines/transportLines.service');
const PassengersService = require('../../../src/passengers/passengers.service');
const debitsCollections = require('../../collections/debits');
const transportLineCollections = require('../../collections/transportLines');
const passengerCollections = require('../../collections/passengers');

const expect = chai.expect;
const debitsService = DebitsService();
const transportLineService = TransportLineService();
const passengersService = PassengersService();

describe('Debits - Unit Tests - Success Cases', () => {
  it('Should return Debit correctly', (done) => {
    const debit = debitsCollections.success[0];
    const tl1 = transportLineCollections.success[0];
    const passenger = passengerCollections.success[0];

    debit.value = 123;
    debit.cardId = 'XnjsoP12';

    passengersService.create(passenger)
      .then(() => transportLineService.create(tl1))
      .then(() => debitsService.create(debit))
      .then((result) => {
        expect(result.value).to.equal(123);
        done();
      });
  });

  it('Should return value debit with 50% discount', (done) => {
    const debit1 = debitsCollections.success[0];
    const debit2 = debitsCollections.success[1];
    const tl1 = transportLineCollections.success[0];
    const tl2 = transportLineCollections.success[1];
    const passenger = passengerCollections.success[0];

    debit2.value = 12;
    debit1.cardId = 'XnjsoP12';
    debit2.cardId = 'XnjsoP12';

    passengersService.create(passenger)
      .then(() => transportLineService.create(tl1))
      .then(() => transportLineService.create(tl2))
      .then(() => debitsService.create(debit1))
      .then(() => debitsService.create(debit2))
      .then((result) => {
        expect(result.value).to.equal(6);
        done();
      })
      .catch(err => console.log(err));
  });

  it('Should not discount at 3 ticket', (done) => {
    const debit1 = debitsCollections.success[0];
    const debit2 = debitsCollections.success[1];
    const debit3 = debitsCollections.success[2];
    const tl1 = transportLineCollections.success[0];
    const tl2 = transportLineCollections.success[1];
    const tl3 = transportLineCollections.success[2];
    const passenger = passengerCollections.success[0];

    debit1.code = '123456';
    debit2.code = '456789';
    debit3.code = '1599357';

    debit1.cardId = 'XnjsoP12';
    debit2.cardId = 'XnjsoP12';
    debit3.cardId = 'XnjsoP12';

    passengersService.create(passenger)
      .then(() => transportLineService.create(tl1))
      .then(() => transportLineService.create(tl2))
      .then(() => transportLineService.create(tl3))
      .then(() => debitsService.create(debit1))
      .then(() => debitsService.create(debit2))
      .then(() => debitsService.create(debit3))
      .then((result) => {
        expect(result.value).to.equal(16);
        done();
      })
      .catch(err => console.log(err));
  });

  it('Should cardId be valid by date', (done) => {
    const debit1 = debitsCollections.success[0];
    const debit2 = debitsCollections.success[1];
    const debit3 = debitsCollections.success[2];
    const tl1 = transportLineCollections.success[0];
    const tl2 = transportLineCollections.success[1];
    const tl3 = transportLineCollections.success[2];
    const passenger = passengerCollections.success[0];

    debit1.code = '123456';
    debit2.code = '456789';
    debit3.code = '1599357';
    debit1.cardId = 'XnjsoP12';
    debit2.cardId = 'XnjsoP12';
    debit3.cardId = 'XnjsoP12';

    passengersService.create(passenger)
      .then(() => transportLineService.create(tl1))
      .then(() => transportLineService.create(tl2))
      .then(() => transportLineService.create(tl3))
      .then(() => debitsService.create(debit1))
      .then(() => debitsService.create(debit2))
      .then(() => debitsService.create(debit3))
      .then((result) => {
        expect(result.value).to.equal(16);
        done();
      })
      .catch(err => console.log(err));
  });
});
