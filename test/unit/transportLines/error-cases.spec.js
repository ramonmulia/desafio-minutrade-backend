const chai = require('chai');
const transportLineCollections = require('../../collections/transportLines');
const TrasnportLineService = require('../../../src/transportLines/transportLines.service');

const expect = chai.expect;
const transportService = TrasnportLineService();

describe('Transport Line - Unit Tests - Error Cases', () => {
  it('Should not be able create transport line when code its already exists', (done) => {
    const tl = transportLineCollections.success[0];
    transportService.create(tl)
      .then(() => transportService.create(tl))
      .catch((err) => {
        expect(err.status).to.equal(400);
        expect(err.error).to.equal('Código da linha já existente');
        done();
      });
  });
});
