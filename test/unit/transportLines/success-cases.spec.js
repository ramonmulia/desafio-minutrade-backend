const chai = require('chai');
const transportLineCollections = require('../../collections/transportLines');
const TrasnportLineService = require('../../../src/transportLines/transportLines.service');

const transportLineService = TrasnportLineService();
const expect = chai.expect;

describe('Transport Line - Unit Tests - Sucess Cases', () => {
  it('Should return Transport Line correctly', (done) => {
    const tl = transportLineCollections.success[0];

    transportLineService.create(tl)
      .then((result) => {
        expect(result.code).to.equal('123456');
        done();
      });
  });
});
