# minutrade-backend-challenge

Pré-requisitos:
    - [Node.js](https://nodejs.org/) v6+.
    - [Mongodb]() v3+

Em sua primeira execução vá na pasta do projeto e execute:
```sh
$ npm i 
$ node start.js
```
Este comando inicia o banco com as seguintes entidades:
- machines:  (máquina de autenticação de débito)
- users: (usuário 'admin' para cadastro de passageiros e linhas de transporte)

Inicia o servidor em ambiente de desenvolvimento:
```sh
$ npm start
```
Executa os testes:
```sh
$ npm test
```

### API

##### POST /api/passengers
Cria passageiro por um usuário admin.
```sh
Autenticação: Basic YWRtaW4wMToxMjM0NTY= 
```

##### GET /api/passengers
Recupera passageiros
```sh
Autenticação: Basic YWRtaW4wMToxMjM0NTY= 
```

##### POST /api/transportlines
Cria linha de transportes por um usuário admin
```sh
Autenticação: Basic YWRtaW4wMToxMjM0NTY= 
```

##### POST /api/debits
Cria débitos por uma máquina existente
```sh
Autenticação: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTcOhcXVpbmEgdGVzdGUiLCJjb2RlIjoibWFjaGluZTAxIiwiaWF0IjoxNTAzMDE2NzY4fQ.7bYUcQ3Kzhmx-5QrV4XpHX7dCfyKhl4iYm1lTtIBAYU
```

##### GET /api/debits
Recupera débitos por um passageiro.
base64 de email:password de um passageiro
```sh
Autenticação: Basic (email:password)
```

### Url pública
 [https://desafio-backend-minutrade.herokuapp.com/](https://desafio-backend-minutrade.herokuapp.com/)

