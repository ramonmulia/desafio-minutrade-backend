const UserService = require('./src/users/users.service');
const MachineService = require('./src/machines/machines.service');
const MongoAdapter = require('./src/adapters/adapters.mongo');

const usersService = UserService();
const machineService = MachineService();
const mongoAdapter = MongoAdapter();


function start() {
  const user = {
    name: 'Usuário admin',
    code: 'admin01',
    password: '123456'
  };

  const machine = {
    name: 'Máquina teste',
    code: 'machine01'
  };
  mongoAdapter.connect()
    .then(() => {
      usersService.create(user)
        .then(() => machineService.create(machine))
        .then(() => {
          console.log('Done!');
          process.exit(0);
        });
    });
}

start();
