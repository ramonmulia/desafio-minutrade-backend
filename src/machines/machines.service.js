const Logger = require('../logger')('./machines/machines.service.js');
const MongoAdapter = require('../adapters/adapters.mongo');
const Cryptography = require('../cryptography/cryptography.service');

const mongoAdapter = MongoAdapter();
const cryptographyService = Cryptography();
const COLLECTION = 'machines';

const MachinesService = {
  create,
  getOneByQuery
};

function getOneByQuery(query) {
  return new Promise((resolve, reject) => {
    const Model = mongoAdapter.getState().collection(COLLECTION);

    return Model.findOne(query || {})
      .then(resolve)
      .catch((err) => {
        Logger.error(`Error when trying get machine by query. Query: ${query} Error: %j ${err}`, err);
        reject({ status: 500, error: 'Houve uma falha ao buscar o passageiro' });
      });
  });
}

function create(payload) {
  return new Promise((resolve, reject) => {
    const machine = Object.assign({}, payload);
    const Model = mongoAdapter.getState().collection(COLLECTION);

    return Model.findOne({ code: machine.code })
      .then((result) => {
        if (result) {
          return reject({ status: 400, error: 'Código máquina existente' });
        }

        machine.token = cryptographyService.encodeToken({ name: machine.name, code: machine.code });

        Model.createIndex({ code: 1 }, { unique: true });

        return Model.insert(machine)
          .then(doc => resolve(doc.ops[0]));
      })
      .catch((err) => {
        Logger.error('Error when trying save machine. Error: %j', err);
        reject({ status: 500, error: 'Houve uma falha ao cadastrar máquina' });
      });
  });
}

module.exports = function factory() {
  return MachinesService;
};
