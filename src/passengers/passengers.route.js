const express = require('express');
const Controller = require('./passengers.controller');
const AuthMiddleware = require('../authorization/authorization.middleware.js');

const router = express.Router();
const controller = Controller();
const authMiddleware = AuthMiddleware();

router.route('/')
  .post(authMiddleware.ensureUserAuthBasic, controller.create)
  .get(authMiddleware.ensureUserAuthBasic, controller.get);

module.exports = router;
