const Schema = {
  title: 'passenger',
  type: 'object',
  properties: {
    name: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    cardId: {
      type: 'string'
    },
    password: {
      type: 'string'
    },
    token: {
      type: 'string'
    }
  },
  required: ['name', 'email', 'cardId', 'password']
};

module.exports = Schema;
