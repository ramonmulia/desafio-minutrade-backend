const moment = require('moment');

function one(passenger) {
  const now = moment().format('DD-MM-YYYY hh:mm:ss');
  const { _id, name, email, cardId, token } = passenger;
  const objReturn = {
    id: _id,
    name,
    email,
    cardId,
    token,
    createdAt: now,
    updatedAt: now,
    lastLogin: now
  };

  return objReturn;
}

function list(passengers) {
  return passengers.map(d => one(d));
}


module.exports = { one, list };
