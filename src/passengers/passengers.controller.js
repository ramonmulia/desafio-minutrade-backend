const Service = require('./passengers.service');
const hal = require('./passengers.hal');

const passengersService = Service();

const PassengerController = {
  create,
  get
};

const DEFAULT_ERROR_MESSAGE = 'Ocorreu um erro no servidor';
const DEFAULT_STATUS_ERROR = 500;

function create(req, res) {
  const { body } = Object.assign({}, req);

  passengersService.create(body)
    .then(result => res.status(201).send(hal.one(result)))
    .catch(err => res.status(err.status || DEFAULT_STATUS_ERROR).send({ error: err.error || DEFAULT_ERROR_MESSAGE }));
}

function get(req, res) {
  const { query } = Object.assign({}, req);

  passengersService.get(query)
    .then((result) => {
      if (!result.length) {
        return res.status(404).send([]);
      }

      return res.status(200).send(hal.list(result));
    })
    .catch(err => res.status(err.status || DEFAULT_STATUS_ERROR).send({ error: err.error || DEFAULT_ERROR_MESSAGE }));
}

module.exports = function factory() {
  return PassengerController;
};
