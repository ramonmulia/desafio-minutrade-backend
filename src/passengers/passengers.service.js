const Logger = require('../logger')('./passengers/passengers.service.js');
const schema = require('./passengers.schema');
const Cryptography = require('../cryptography/cryptography.service');
const tv4 = require('tv4');
const MongoQS = require('mongo-querystring');
const MongoAdapter = require('../adapters/adapters.mongo');

const mongoQs = new MongoQS();
const mongoAdapter = MongoAdapter();
const cryptographyService = Cryptography();
const COLLECTION = 'passengers';

const PassengersService = {
  create,
  getOneByQuery,
  get
};

function create(payload) {
  return new Promise((resolve, reject) => {
    const valid = tv4.validate(payload, schema);
    const passenger = Object.assign({}, payload);
    const Model = mongoAdapter.getState().collection(COLLECTION);

    if (!valid) {
      return reject({ status: 400, error: 'Payload inválido.' });
    }

    passenger.email = passenger.email.toLowerCase().trim();

    return Model.findOne({ $or: [{ email: passenger.email }, { cardId: passenger.cardId }] })
      .then((result) => {
        if (result) {
          if (result.email === passenger.email) {
            return reject({ status: 400, error: 'E-mail já existente' });
          }
          return reject({ status: 400, error: 'CardId já existente' });
        }

        passenger.password = cryptographyService.encryptPassword(passenger.password);
        passenger.token = cryptographyService.encodeToken({ name: passenger.name, email: passenger.email });

        Model.createIndex({ email: 1 }, { unique: true });
        Model.createIndex({ cardId: 2 }, { unique: true });

        return Model.insert(passenger)
          .then(doc => resolve(doc.ops[0]));
      })
      .catch((err) => {
        Logger.error(`Error when trying save passenger. Error: %j ${err}`, err);
        reject({ status: 500, error: 'Houve uma falha ao cadastrar o passageiro' });
      });
  });
}

function getOneByQuery(query) {
  return new Promise((resolve, reject) => {
    const Model = mongoAdapter.getState().collection(COLLECTION);

    return Model.findOne(query || {})
      .then(resolve)
      .catch((err) => {
        Logger.error(`Error when trying get passenger by email. Error: %j ${err}`, err);
        reject({ status: 500, error: 'Houve uma falha ao buscar o passageiro' });
      });
  });
}

function get(query) {
  return new Promise((resolve, reject) => {
    const queryMongo = mongoQs.parse(query) || {};
    const Model = mongoAdapter.getState().collection(COLLECTION);

    Model.find(queryMongo)
      .toArray((err, passengers) => {
        if (err) {
          Logger.error(`Error when trying to get passengers. Error: ${err} %j`, err);
          return reject({ status: 400, error: 'Houve uma falha ao buscar os passageiros com os parametros passados.' });
        }

        return resolve(passengers);
      });
  });
}

module.exports = function factory() {
  return PassengersService;
};
