const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { SECRET } = require('../config/config.crypto');

const CryptoService = {
  comparePassword,
  encryptPassword,
  encodeToken,
  decodeToken
};

function encryptPassword(password) {
  const salt = bcrypt.genSaltSync();
  const hash = bcrypt.hashSync(password, salt);

  return hash;
}

function comparePassword(password, passwordToCompare) {
  const isEqual = bcrypt.compareSync(password, passwordToCompare);

  return isEqual;
}

function encodeToken(obj) {
  const token = jwt.sign(obj, SECRET);

  return token;
}

function decodeToken(token) {
  const decoded = jwt.verify(token, SECRET);

  return decoded;
}

module.exports = function factory() {
  return CryptoService;
};
