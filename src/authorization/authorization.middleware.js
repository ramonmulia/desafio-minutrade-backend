const basicAuth = require('basic-auth');
const Cryptography = require('../cryptography/cryptography.service');
const PassengersService = require('../../src/passengers/passengers.service');
const MachineService = require('../../src/machines/machines.service');
const UserService = require('../../src/users/users.service');

const cryptographyService = Cryptography();
const passengersService = PassengersService();
const machineService = MachineService();
const userService = UserService();

const DEFAULT_MACHINE_ERROR_MESSAGE = 'Máquina não autenticada.';
const DEFAULT_PASSENGER_MESSAGE = 'Passageiro não autenticado.';
const DEFAULT_USER_ERROR_MESSAGE = 'Usuário não autenticado';

const AuthorizationMidleware = {
  ensurePassengerAuthBasic,
  ensureMachineAuthBearer,
  ensureUserAuthBearer,
  ensureUserAuthBasic
};

function ensurePassengerAuthBasic(req, res, next) {
  const { headers } = Object.assign({}, req);
  const credentials = basicAuth.parse(headers.authorization);

  if (!credentials) {
    return res.status(401).send({ error: DEFAULT_PASSENGER_MESSAGE });
  }

  try {
    return passengersService.getOneByQuery({ email: credentials.name })
      .then((pasenger) => {
        if (!pasenger) {
          return res.status(401).send({ error: DEFAULT_PASSENGER_MESSAGE });
        }

        const isPasswordCorrect = cryptographyService.comparePassword(credentials.pass, pasenger.password);

        if (!isPasswordCorrect) {
          return res.status(401).send({ error: DEFAULT_PASSENGER_MESSAGE });
        }
        return next();
      });
  } catch (err) {
    return res.status(401).send({ error: DEFAULT_PASSENGER_MESSAGE });
  }
}

function ensureUserAuthBasic(req, res, next) {
  const { headers } = Object.assign({}, req);
  const credentials = basicAuth.parse(headers.authorization);

  if (!credentials) {
    return res.status(401).send({ error: DEFAULT_USER_ERROR_MESSAGE });
  }

  try {
    return userService.getOneByQuery({ code: credentials.name })
      .then((user) => {
        if (!user) {
          return res.status(401).send({ error: DEFAULT_USER_ERROR_MESSAGE });
        }

        const isPasswordCorrect = cryptographyService.comparePassword(credentials.pass, user.password);

        if (!isPasswordCorrect) {
          return res.status(401).send({ error: DEFAULT_USER_ERROR_MESSAGE });
        }
        return next();
      });
  } catch (err) {
    return res.status(401).send({ error: DEFAULT_USER_ERROR_MESSAGE });
  }
}

function ensureMachineAuthBearer(req, res, next) {
  const { headers } = Object.assign({}, req);
  const machine = decodeTokenByHeader(headers);

  if (!machine) {
    return res.status(401).send({ error: DEFAULT_MACHINE_ERROR_MESSAGE });
  }

  return machineService.getOneByQuery({ code: machine.code })
    .then((result) => {
      if (!result) {
        return res.status(401).send({ error: DEFAULT_MACHINE_ERROR_MESSAGE });
      }
      return next();
    });
}

function ensureUserAuthBearer(req, res, next) {
  const { headers } = Object.assign({}, req);
  const user = decodeTokenByHeader(headers);

  if (!user) {
    return res.status(401).send({ error: DEFAULT_USER_ERROR_MESSAGE });
  }

  return userService.getOneByQuery({ code: user.code })
    .then((result) => {
      if (!result) {
        return res.status(401).send({ error: DEFAULT_USER_ERROR_MESSAGE });
      }
      return next();
    });
}

function decodeTokenByHeader(headers) {
  if (!(headers && headers.authorization)) {
    return null;
  }

  try {
    const header = headers.authorization.split(' ');
    const token = header[1];
    const entity = cryptographyService.decodeToken(token);

    return entity;
  } catch (err) {
    return null;
  }
}

module.exports = function factory() {
  return AuthorizationMidleware;
};
