const exportConfig = require('export-config');

const config = {
  default: {
    SECRET: 'backend-challenge'
  },
  development: {
    SECRET: 'backend-challenge'
  },
  staging: {
    SECRET: 'backend-challenge'
  },
  production: {
    SECRET: 'backend-challenge'
  },
  required: ['SECRET']
};

module.exports = exportConfig(config);
