const exportConfig = require('export-config');

const config = {
  default: {
    PORT: 3000,
    SECRET: 'backend-challenge'
  },
  development: {
    PORT: 3000,
    SECRET: 'backend-challenge'
  },
  staging: {
    PORT: 3000,
    SECRET: 'backend-challenge'
  },
  production: {
    PORT: process.env.PORT,
    SECRET: 'backend-challenge'
  },
  required: ['PORT', 'SECRET']
};

module.exports = exportConfig(config);
