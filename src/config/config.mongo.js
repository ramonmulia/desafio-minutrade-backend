const exportConfig = require('export-config');

const config = {
  development: {
    URI: 'mongodb://localhost:27017/backend-challenge'
  },
  production: {
    URI: 'mongodb://challengeuser:123456@ds113628.mlab.com:13628/backend-challenge'
  },
  staging: {
    URI: process.env.MONGO_ADDRESS
  },
  test: {
    URI: 'mongodb://localhost:27017/backend-challenge-test'
  },
  required: ['URI']
};

module.exports = exportConfig(config);
