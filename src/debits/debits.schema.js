const Schema = {
  title: 'passenger',
  type: 'object',
  properties: {
    code: {
      type: 'string'
    },
    cardId: {
      type: 'string'
    },
    value: {
      type: 'number'
    }
  },
  required: ['code', 'cardId', 'value']
};

module.exports = Schema;
