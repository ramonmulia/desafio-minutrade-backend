const Service = require('./debits.service');
const hal = require('./debits.hal');

const debitsService = Service();

const DebitsController = {
  create,
  get
};

const DEFAULT_ERROR_MESSAGE = 'Ocorreu um erro no servidor';
const DEFAULT_STATUS_ERROR = 500;

function create(req, res) {
  const { body } = Object.assign({}, req);

  debitsService.create(body)
    .then(result => res.status(201).send(hal.one(result)))
    .catch(err => res.status(err.status || DEFAULT_STATUS_ERROR).send({ error: err.error || DEFAULT_ERROR_MESSAGE }));
}

function get(req, res) {
  const { query } = req;

  debitsService.get(query)
    .then((result) => {
      if (!result.length) {
        return res.status(404).send([]);
      }

      return res.status(200).send(hal.list(result));
    })
    .catch(err => res.status(err.status || DEFAULT_STATUS_ERROR).send({ error: err.error || DEFAULT_ERROR_MESSAGE }));
}

module.exports = function factory() {
  return DebitsController;
};
