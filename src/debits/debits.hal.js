function one(debit) {
  return { id: debit._id };
}

function all(debit) {
  const { _id, debitedAt, value } = debit;
  return { id: _id, debitedAt, value };
}

function list(debits) {
  return debits.map(d => all(d));
}


module.exports = { one, list };
