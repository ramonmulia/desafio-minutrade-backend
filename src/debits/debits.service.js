const tv4 = require('tv4');
const schema = require('./debits.schema');
const Logger = require('../logger')('./debits/service.js');
const MongoQS = require('mongo-querystring');
const MongoAdapter = require('../adapters/adapters.mongo');
const TrasnportLinesService = require('../transportLines/transportLines.service');
const PassengersService = require('../passengers/passengers.service');

const COLLECTION = 'debits';
const DISCOUNT_PERCENT = 50;
const LIMIT_SIZE_DEBIT_PER_DAY = 6;

const mongoQs = new MongoQS();
const moment = global.moment;
const mongoAdapter = MongoAdapter();
const transportLineService = TrasnportLinesService();
const passengersService = PassengersService();

const DebitService = {
  create,
  get
};

function create(payload) {
  return new Promise((resolve, reject) => {
    const Model = mongoAdapter.getState().collection(COLLECTION);
    const valid = tv4.validate(payload, schema);
    const debit = Object.assign({}, payload);
    const now = moment(moment().format('YYYY-MM-DD')).toDate();

    if (!valid) {
      return reject({ status: 400, error: 'Payload inválido.' });
    }

    debit.debitedAt = moment().toDate();

    return passengersService.getOneByQuery({ cardId: debit.cardId })
      .then((psg) => {
        if (!psg) {
          Logger.error('Passenger not found!');
          return reject({ status: 400, error: 'Houve uma falha ao cadastrar o debito, cartão não encontrado.' });
        }
        return transportLineService.findByCode(debit.code);
      })
      .then((tl) => {
        if (!tl) {
          Logger.error('Transport line not found!');
          return reject({ status: 400, error: 'Houve uma falha ao cadastrar o debito, linha de transporte não encotrada.' });
        }
        return validateLimitPerDay(now, debit.cardId);
      })
      .then((debits) => {
        const hourNow = moment().format('HH');
        debit.value = setDebitDiscount(now, hourNow, debits, debit);
        return Model.insert(debit);
      })
      .then(doc => resolve(doc.ops[0]))
      .catch((err) => {
        Logger.error(`Error when trying to save debit. Error: ${err} %j`, err);
        if (!err.status) {
          return reject({ status: 500, error: 'Houve uma falha ao cadastrar o debito.' });
        }

        return reject(err);
      });
  });
}

function get(query) {
  return new Promise((resolve, reject) => {
    const queryMongo = parseQuery(query);
    const Model = mongoAdapter.getState().collection(COLLECTION);

    Model.find(queryMongo)
      .toArray((err, debits) => {
        if (err) {
          Logger.error(`Error when trying to get debit. Error: ${err} %j`, err);
          return reject({ status: 400, error: 'Houve uma falha ao buscar o débito com os parametros passados.' });
        }

        return resolve(debits);
      });
  });
}

function parseQuery(query) {
  let queryMongo = {};

  if (query) {
    queryMongo = mongoQs.parse(query);

    if (queryMongo.debitedAt) {
      const sterOfDay = moment(queryMongo.debitedAt).toDate();
      const endOfDay = moment(queryMongo.debitedAt).endOf('day').toDate();

      queryMongo.debitedAt = {
        $gte: sterOfDay,
        $lte: endOfDay
      };
    }
  }

  return queryMongo;
}

function validateLimitPerDay(now, cardId) {
  return new Promise((resolve, reject) => {
    const Model = mongoAdapter.getState().collection(COLLECTION);

    Model.find({ cardId, debitedAt: { $gte: now } })
      .toArray((err, result) => {
        if (result.length === LIMIT_SIZE_DEBIT_PER_DAY) {
          Logger.error('Limit reached!');
          return reject({ status: 400, error: 'Limite de uso por dia estourado!' });
        }

        return resolve(result);
      });
  });
}

function setDebitDiscount(date, hour, debits, debit) {
  if (debits.length === 1) {
    const firstDebit = debits[0];
    const debitedAtMoment = moment(firstDebit.debitedAt);

    if (firstDebit.code === debit.code) {
      return debit.value;
    }

    if (debitedAtMoment.format('DD-MM-YYYY') === moment(date).format('DD-MM-YYYY')) {
      const debitHour = debitedAtMoment.format('HH');

      if (debitHour === hour) {
        return (debit.value * DISCOUNT_PERCENT) / 100;
      }
    }
  }

  return debit.value;
}

module.exports = function factory() {
  return DebitService;
};
