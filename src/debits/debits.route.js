const express = require('express');
const Controller = require('./debits.controller');
const AuthMiddleware = require('../authorization/authorization.middleware.js');

const router = express.Router();
const controller = Controller();
const authMiddleware = AuthMiddleware();

router.route('/')
  .post(authMiddleware.ensureMachineAuthBearer, controller.create)
  .get(authMiddleware.ensurePassengerAuthBasic, controller.get);

module.exports = router;
