const Logger = require('../logger')('./users/users.service.js');
const MongoAdapter = require('../adapters/adapters.mongo');
const Cryptography = require('../cryptography/cryptography.service');

const mongoAdapter = MongoAdapter();
const cryptographyService = Cryptography();
const COLLECTION = 'users';

const UsersService = {
  getOneByQuery,
  create
};

function getOneByQuery(query) {
  return new Promise((resolve, reject) => {
    const Model = mongoAdapter.getState().collection(COLLECTION);

    return Model.findOne(query || {})
      .then(resolve)
      .catch((err) => {
        Logger.error(`Error when trying get machine by query. Query: ${query} Error: %j ${err}`, err);
        reject({ status: 500, error: 'Houve uma falha ao buscar o usuário' });
      });
  });
}

function create(payload) {
  return new Promise((resolve, reject) => {
    const user = Object.assign({}, payload);
    const Model = mongoAdapter.getState().collection(COLLECTION);

    user.password = cryptographyService.encryptPassword(user.password);
    user.token = cryptographyService.encodeToken({ name: user.name, code: user.code });

    return Model.findOne({ code: user.code })
      .then((result) => {
        if (result) {
          return reject({ status: 400, error: 'Usuário existente.' });
        }

        Model.createIndex({ code: 1 }, { unique: true });

        return Model.insert(user)
          .then(doc => resolve(doc.ops[0]));
      })
      .catch((err) => {
        Logger.error('Error when trying save user. Error: %j', err);
        reject({ status: 500, error: 'Houve uma falha ao cadastrar o usuário' });
      });
  });
}

module.exports = function factory() {
  return UsersService;
};
