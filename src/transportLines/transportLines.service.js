const Logger = require('../logger')('./transportLines/transportLines.service.js');
const schema = require('./transportLines.schema');
const tv4 = require('tv4');
const MongoAdapter = require('../adapters/adapters.mongo');

const mongoAdapter = MongoAdapter();
const COLLECTION = 'transport_lines';

const TrasnportLineService = {
  create,
  findByCode
};

function create(payload) {
  return new Promise((resolve, reject) => {
    const valid = tv4.validate(payload, schema);
    const transportLine = Object.assign({}, payload);
    const Model = mongoAdapter.getState().collection(COLLECTION);

    if (!valid) {
      return reject({ status: 400, error: 'Payload inválido.' });
    }

    return Model.findOne({ code: transportLine.code })
      .then((result) => {
        if (result) {
          return reject({ status: 400, error: 'Código da linha já existente' });
        }

        Model.createIndex({ code: 1 }, { unique: true });

        return Model.insert(transportLine)
          .then(doc => resolve(doc.ops[0]));
      })
      .catch((err) => {
        Logger.error('Error when trying save transport line. Error: %j', err);
        reject({ status: 500, error: 'Houve uma falha ao cadastrar o a linha de transporte' });
      });
  });
}

function findByCode(code) {
  return new Promise((resolve, reject) => {
    const Model = mongoAdapter.getState().collection(COLLECTION);

    return Model.findOne({ code })
      .then(result => resolve(result))
      .catch((err) => {
        Logger.error('Error when trying find by code transport line. Error: %j', err);
        reject({ status: 500, error: 'Houve uma falha ao buscar linha de transporte pelo código.' });
      });
  });
}

module.exports = function factory() {
  return TrasnportLineService;
};
