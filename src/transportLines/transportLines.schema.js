const Schema = {
  title: 'transportLine',
  type: 'object',
  properties: {
    code: {
      type: 'string'
    },
    origin: {
      type: 'string'
    },
    destination: {
      type: 'string'
    }
  },
  required: ['code', 'origin', 'destination']
};

module.exports = Schema;
