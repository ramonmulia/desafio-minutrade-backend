const express = require('express');
const Controller = require('./transportLines.controller');
const AuthMiddleware = require('../authorization/authorization.middleware.js');

const router = express.Router();
const controller = Controller();
const authMiddleware = AuthMiddleware();

router.route('/')
  .post(authMiddleware.ensureUserAuthBasic, controller.create);

module.exports = router;
