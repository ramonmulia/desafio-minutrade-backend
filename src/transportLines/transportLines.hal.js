function one(transportLine) {
  return { id: transportLine._id };
}

module.exports = { one };
