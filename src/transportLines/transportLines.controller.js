const Service = require('./transportLines.service');
const hal = require('./transportLines.hal');

const tlService = Service();

const TransportLineController = {
  create
};

const DEFAULT_ERROR_MESSAGE = 'Ocorreu um erro no servidor.';
const DEFAULT_STATUS_ERROR = 500;

function create(req, res) {
  const { body } = Object.assign({}, req);

  tlService.create(body)
    .then(result => res.status(201).send(hal.one(result)))
    .catch(err => res.status(err.status || DEFAULT_STATUS_ERROR).send({ error: err.error || DEFAULT_ERROR_MESSAGE }));
}

module.exports = function factory() {
  return TransportLineController;
};
